# A WEBSITE PROJECT
Deploy an application to EC2 using Docker

## Technology
* Ansible
* EC2
* Docker
* Git, Gitlab

## Setup Prerequisites
* A ssh key in .ssh/
* Ansible [install](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-22-04)
    ```
    $ sudo apt install ansible
    ```
* AWS account

## Quick Start
Create EC2
```
Create EC2 t2.micro and set security group allow port HTTP 80 and 4000
```

Build docker image
```
$ docker build -t registry.gitlab.com/lamkhoi1304/a-website:latest .
```
Setup EC2
```
$ cd ansible
$ ansible-playbook -i inventory.yml docker-playbook.yml
```
Deploy monitoring
```
$ ansible-playbook -i inventory.yml monitoring-playbook.yml
Access public IP port 4000
Import ID 193
```
Deploy application to EC2
```
$ ansible-playbook -i inventory.yml deploy-playbook.yml
```
Verify
```
Access public IP EC2
```
